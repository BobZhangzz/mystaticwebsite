+++
title = "Developed JAMBO MoodTunes: Mood-Based Song Recommendations on Azure"
date = 2023-12-11
updated = 2024-01-31
description = "Play the songs in favour of your mood"

[taxonomies]
tags = ["showcase", "tutorial", "FAQ"]

[extra]
footnote_backlinks = true
quick_navigation_buttons = true
+++
