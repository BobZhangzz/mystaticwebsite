+++
title = "Text Embedding Visualization"
description = "Projecting Vectorized Texts in 2D using tSNE, PCA, and UMAP"
weight = 2

[extra]
local_image = "projects/text\ embedding\ viz/text_embed.webp"
#canonical_url = "https://osc.garden/projects/text\ embedding/"
#social_media_card = "social_cards/projects_chu.jpg"
toc = true
+++

Contextual Document Embeddings (CDE) are advanced representations that capture the nuanced meanings of documents by considering the context of each word within the document and its surrounding sentences. Unlike traditional document embeddings, which often average or aggregate word vectors without accounting for word order or sentence structure, CDE leverages deep learning models like Transformer-based architectures (e.g., BERT, GPT, RoBERTa) to understand semantic dependencies and relationships between words.

In practice, CDE represents a document as a dense vector, typically generated by encoding each sentence with attention to word positions, syntactic roles, and contextual associations across the document. This allows for richer embeddings that reflect changes in meaning based on context, helping CDE handle tasks such as semantic similarity, document classification, information retrieval, and summarization with greater accuracy.

The latest model, [**cde-small-v1**](https://huggingface.co/jxm/cde-small-v1), introduces a novel approach by naturally integrating "context tokens" into the embedding process. This enables it to capture deeper semantic relationships across various contexts more effectively than previous models. As of October 1, 2024, **cde-small-v1** ranks as the top-performing small model (under 400 million parameters) on the Massive Text Embedding Benchmark (MTEB) leaderboard for text embedding models, achieving an impressive average score of 65.00.

This integration of "context tokens" allows **cde-small-v1** to provide contextually rich embeddings even with a compact architecture, making it highly efficient and effective for a broad range of tasks requiring semantic understanding. hi 